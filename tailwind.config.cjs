/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "styled": "linear-gradient(180deg, #ff008c 0%, rgb(211, 9, 225) 100%)",
        "styled-circular": "radial-gradient(circle, #ff008c 0%, rgb(211, 9, 225) 100%)",
      },
      backgroundClip: {
        "styled": "linear-gradient(180deg, #ff008c 0%, rgb(211, 9, 225) 100%)",
      }
    },
  },
  plugins: [],
}
