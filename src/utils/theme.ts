import { createTheme } from "@mui/material";
import { pink } from "@mui/material/colors";
import blue from "@mui/material/colors/blue";
import green from "@mui/material/colors/green";
import grey from "@mui/material/colors/grey";
import red from "@mui/material/colors/red";

export const theme = createTheme({
    palette: {
        primary: {
            main: pink["A400"],
            contrastText: grey[50]
        },
        error: {
            main: red["A400"],
            contrastText: grey[50]
        },
        success: {
            main: green["A400"],
            // color text 
            contrastText: grey[50]
        },
        info: {
            main: blue[500],
            contrastText: grey[50]
        },
        secondary: {
            main: grey[50],
            contrastText: pink["A400"],
            // hover property 
        },
    },
    typography: {
        fontWeightLight: 300,
        fontWeightRegular: 400,
        fontWeightMedium: 500,
        fontWeightBold: 700,
    },
});