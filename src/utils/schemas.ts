import * as yup from 'yup';

export const feedSchema = yup.object().shape({
    title: yup.string().required('Title is required').min(3, 'Title must be at least 3 characters').max(25, 'Title must be less than 25 characters'),
    description: yup.string().required('Description is required').min(15, 'Description must be at least 15 characters').max(75, 'Description must be less than 100 characters'),
})