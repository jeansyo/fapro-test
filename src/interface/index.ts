import { EntityState } from "@reduxjs/toolkit";

export interface FeedState extends EntityState<FeedBaseProps> {
    loading: 'idle' | 'pending' | 'succeeded' | 'failed',
    editFeed: FeedBaseProps | null,
    removeFeed: FeedBaseProps | null,
    newFeed: boolean | null,
}


export interface FeedBaseProps {
    title: string,
    description: string,
    date?: number | null | undefined,
    id?: string | null | undefined,
}


export interface JeanSyoResponseProps {
    date: Date,
    code: number,
    message: string,
    success: boolean,
    data?: FeedBaseProps | Array<FeedBaseProps> | null | undefined,
}

export interface JeanSyoResponse {
    jeansyo: JeanSyoResponseProps
}


export interface ModalStyledProps {
    show: boolean,
    onShow: () => void,
    children: React.ReactNode,
}

export interface TextFieldStyledProps {
    label: string,
    name: string,
    type: string,
    variant?: undefined | "standard" | "filled" | "outlined",
    placeholder?: string,
    multiline?: boolean,
    rows?: number,
    maxLenght?: number,
    disabled?: boolean,
}

export interface LayoutStyledProps {
    children: React.ReactNode
}

export interface LoadingStyledProps {
    children?: React.ReactNode
}

