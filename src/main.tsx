import ThemeProvider from '@mui/material/styles/ThemeProvider'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux/es/exports'
import App from './App'
import './index.css'
import store from './redux/store'
import { theme } from './utils/theme'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider
      store={store}
    >
      <ThemeProvider
        theme={theme}
      >
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
)
