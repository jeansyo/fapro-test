import React, { useEffect, useState } from 'react'
import { Box } from '@mui/material'
import TextField from '@mui/material/TextField/TextField'
import { useField } from 'formik'
import { motion, useAnimation, transform } from 'framer-motion'
import { TextFieldStyledProps } from '../../interface'

const mapRemainingToColor = transform([2, 6], ["#ff008c", "#ccc"]);
const mapRemainingToSpringVelocity = transform([0, 5], [50, 0]);


export default function TextFieldStyled({
    label,
    name,
    type,
    variant="outlined",
    placeholder,
    multiline=false,
    rows=1,
    maxLenght=12,
    disabled=false,
}: TextFieldStyledProps) {

    const [
        field,
        meta,
        helpers,
    ] = useField(name)

    const controls = useAnimation();


    const charactersRemaining = maxLenght - field?.value?.length || 0;


    useEffect(() => {
        if (charactersRemaining > maxLenght/2) return;
        controls.start({
            scale: 1,
            transition: {
            type: "spring",
            velocity: mapRemainingToSpringVelocity(charactersRemaining),
            stiffness: 700,
            damping: 80
            }
        });
    }, [field.value]);

  return (
    <Box>
        <TextField
            label={label}
            name={name}
            type={type}
            variant={variant}
            value={field.value}
            onChange={
                (e) => {
                    helpers.setValue(e.target.value)
                }
            }
            onBlur={
                () => {
                    helpers.setTouched(true)
                }
            }
            error={meta.touched && Boolean(meta.error)}
            helperText={meta.touched && meta.error}
            fullWidth
            placeholder={placeholder}
            multiline={multiline}
            rows={rows}
            disabled={disabled}
            InputProps={{
                endAdornment: (
                    <>
                        <motion.span
                            style={{ color: mapRemainingToColor(charactersRemaining) }}
                            animate={controls}
                        >
                            {charactersRemaining}
                        </motion.span>
                    </>
                )
            }}
        />
    </Box>
  )
}
