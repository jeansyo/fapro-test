import { Avatar } from '@mui/material'
import { motion } from 'framer-motion'
import dayjs from 'dayjs'
import pink from '@mui/material/colors/pink'
import Typography from '@mui/material/Typography'
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';

export default function HeaderStyled() {
  return (
    <motion.header
        className="flex flex-row justify-between items-end border-b-2 border-gray-200 py-2 mb-2 px-2"
        initial={{ y: -50, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        transition={{ duration: 0.5 }}
    >
        <motion.div
            className='flex flex-col gap-1'
        >
            <Typography
                variant='caption'
                color="text.secondary"
                className='uppercase text-xs tracking-widest'
            >
                {
                    new Date().toLocaleDateString('en-US', {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    })
                } 
            </Typography>
            <motion.h1
                className="text-3xl font-bold bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-purple-500"
            >
                {
                    dayjs().format('dddd')
                }
            </motion.h1>
        </motion.div>
        <Avatar
            component={motion.div}
            animate={{
                scale: [1, 1.025, 1.025, 1, 1],
                rotate: [0, 0, 180, 270, 0],
                borderRadius: ["10%", "25%", "50%", "50%", "15%"]
            }}
            exit={{
                scale: [1, 0],
                rotate: [0, 180],
                borderRadius: ["0%", "50%"]
            }}
            transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.2, 0.5, 0.8, 1],
                repeat: Infinity,
                repeatDelay: 1,
            }}
            className="bg-styled-circular"
        >
            <FiberManualRecordIcon />
        </Avatar>
    </motion.header>
  )
}
