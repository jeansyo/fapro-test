import {
  Fragment,
  useState
} from 'react'

import {
  Button,
  Card, 
  CardActions, 
  CardContent, 
  Typography
} from '@mui/material'
import { motion } from 'framer-motion'
import { setEditFeed, setRemoveFeed, _feedDeleteFeedById } from '../../redux/slices/feedSlice'
import { useAppDispatch } from '../../redux/store'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { FeedBaseProps } from '../../interface'
import pink from '@mui/material/colors/pink'
dayjs.extend(relativeTime)


const item = {
  hidden: {
    opacity: 0,
    y: 50,
    minHeight: "9.5rem",
  },
  show: {
    opacity: 1,
    y: 0,
    minHeight: "9.5rem",
  },
  exit: {
    opacity: 0,
    minHeight: "0rem",
  }
}

export default function CardStyled(card: FeedBaseProps) {

  const dispatch = useAppDispatch()


  const onShowDelete = () => {
    dispatch(setRemoveFeed(card))
  }

  const onShowEdit = () => {
    dispatch(setEditFeed(card))
  }

  return (
      <Card
          sx={{
            width: "100%",
            borderRadius: ".7rem",
            boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
          }}
          component={motion.div}
          exit="exit"
          initial="hidden"
          animate="show"
          variants={item}
          transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98], staggerChildren: 0.2 }}
          className="relative cursor-pointer shadow-lg bg-styled"
          whileHover={{ scale: 1.03 }}
          whileTap={{ scale: 0.98 }}
      >
          <Typography
            variant="caption"
            component="div"
            className="text-white absolute bottom-[.75rem] right-[.75rem]"
          >
            {
              // make from now with dayjs valid for typescript 
              dayjs(card.date).fromNow()
            }
          </Typography>
          <CardContent>
              <Typography 
                gutterBottom 
                variant="h5" 
                component="div"
                className='text-white text-limit-2'
              >
                {
                  card.title
                }
              </Typography>
              <Typography 
                variant="body2" 
                className='text-white text-limit-2'
              >
                {
                  card.description
                }
              </Typography>
          </CardContent>
          <CardActions
            className='absolute bottom-[.25rem] left-[.25rem] right-0'
          >
            <Button 
              size="small"
              color='secondary'
              variant='contained'
              onClick={onShowEdit}
              disableElevation
            >
              Edit
            </Button>
            <Button 
              size="small"
              color='secondary'
              variant='contained'
              disableElevation
              onClick={onShowDelete}
            >
              Remove
            </Button>
          </CardActions>
      </Card>
  )
}
