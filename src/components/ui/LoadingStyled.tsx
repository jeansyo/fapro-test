import { motion } from 'framer-motion'
import { LoadingStyledProps } from '../../interface'

export const LoadingStyled = ({
  children,
}:LoadingStyledProps) => {
  return (
    <motion.div
        className="flex flex-col items-center justify-center h-full w-full"
        >
        <motion.div
            className="w-[5rem] h-[5rem] bg-styled-circular"
            animate={{
            scale: [1, 2, 2, 1, 1],
            rotate: [0, 0, 180, 180, 0],
            borderRadius: ["10%", "25%", "50%", "35%", "15%"]
            }}
            exit={{
            scale: [1, 0],
            rotate: [0, 180],
            borderRadius: ["0%", "50%"]
            }}
            transition={{
            duration: 2,
            ease: "easeInOut",
            times: [0, 0.2, 0.5, 0.8, 1],
            repeat: Infinity,
            repeatDelay: 1
            }}
        />
        {
          !!children && children
        }
    </motion.div>
  )
}
