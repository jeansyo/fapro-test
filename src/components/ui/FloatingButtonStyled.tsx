import {
    Fragment,
    useState
} from 'react'

import { Fab } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';
import {
    motion
} from 'framer-motion'
import { useAppDispatch } from '../../redux/store';
import { setNewFeed, _feedAddNewFeed } from '../../redux/slices/feedSlice';

export default function FloatingButtonStyled() {

    const dispatch = useAppDispatch()

    const onShow = () => {
        dispatch(setNewFeed(true))
    }

   
  return (
    <Fragment>
        <motion.div
            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.95 }}
            transition={{ type: "spring", stiffness: 400, damping: 17 }}
            style={{
                position: "fixed",
                bottom: "2rem",
                right: "2rem",
                zIndex: 100,    
            }}
            onClick={onShow}
            initial={{ opacity: 0 }}
            animate={{
                opacity: 1,
                transition: {
                    delay: 0.5,
                    duration: 0.5,
                }
            }}
        >
            <Fab 
                color="primary"
            >
                <AddIcon />
            </Fab>
        </motion.div>
    </Fragment>
  )
}
