import { Box, Modal } from '@mui/material'
import { motion } from 'framer-motion'
import React from 'react'

import { ModalStyledProps } from '../../interface';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: 4,
};

export const ModalStyled = ({
    show,
    onShow,
    children,
}: ModalStyledProps) => {
  return (
    <Modal
        open={show}
        onClose={onShow}
    >
        <Box
            sx={style}
            component={motion.div}
            className="max-w-sm w-11/12"
        >
            {
                children
            }
        </Box> 
    </Modal>
  )
}
