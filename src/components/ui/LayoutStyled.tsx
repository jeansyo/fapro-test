import { Fragment } from 'react'
import { Box, Typography } from '@mui/material'
import { LayoutStyledProps } from '../../interface'
import FloatingButtonStyled from './FloatingButtonStyled'
import HeaderStyled from './HeaderStyled'
import {useSelector} from 'react-redux'
import { selectLoadingFeed } from '../../redux/slices/feedSlice'
import { AnimatePresence, motion } from 'framer-motion'
import pink from '@mui/material/colors/pink'
import { LoadingStyled } from './LoadingStyled'

export default function LayoutStyled({
    children,
}: LayoutStyledProps
) {

  const loading = useSelector<boolean>(selectLoadingFeed)

  return (
    <div
      className='flex items-center justify-center h-screen w-screen overflow-hidden bg-'
    >
      <Box
        className="rounded-none sm:rounded-[2.5rem] w-full h-screen max-w-sm sm:h-[90vh] p-4 bg-white overflow-y-auto overflow-x-visible position-relative"
        sx={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          boxShadow: "0 50px 100px -20px rgb(50 50 93 / 25%), 0 30px 60px -30px rgb(0 0 0 / 30%), inset 0 -2px 6px 0 rgb(10 37 64 / 35%)"
        }}
      >
        {
          loading
            ? (
              <LoadingStyled/>
            ): (
              <Fragment >
                <HeaderStyled />
                <FloatingButtonStyled />
                {
                    children
                }
              </Fragment>
            )
        }
        
      </Box>
    </div>
  )
}
