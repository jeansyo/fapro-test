import Typography from '@mui/material/Typography'
import React from 'react'
import { ModalStyled } from '../ui/ModalStyled'
import { Formik } from 'formik'
import { Box } from '@mui/system'
import TextFieldStyled from '../ui/TextFieldStyled'
import Button from '@mui/material/Button'
import { clearEditFeed, selectEditFeed, _feedUpdateFeedById } from '../../redux/slices/feedSlice'
import { useAppDispatch } from '../../redux/store'
import { useSelector } from 'react-redux/es/exports'
import { FeedBaseProps } from '../../interface'
import { feedSchema } from '../../utils/schemas'
import CircularProgress from '@mui/material/CircularProgress'
export default function FeedEditModal() {

    const dispatch = useAppDispatch()
    const currentFeed =  useSelector<FeedBaseProps, FeedBaseProps>(selectEditFeed)

    const onShowEdit = () => {
        dispatch(clearEditFeed())
    }

    const handleOnEdit = (
        values: FeedBaseProps, 
        { setSubmitting }: any
    ) => {
        setSubmitting(true)
        dispatch(_feedUpdateFeedById(values))
          .then(() => {
            onShowEdit()
          })
          .catch((err) => {
            console.log(err)
          })
          .finally(() => {
            setSubmitting(false)
          })
    }

  return (
    <ModalStyled
        show={!!currentFeed}
        onShow={onShowEdit}
    >
        <Typography 
            gutterBottom 
            variant="subtitle2"
            component="div"
            className='font-bold uppercase bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-purple-500'
        >
            Edit Feed
        </Typography>
        <Typography 
            variant="body2" 
            color="text.secondary"
        >
            Edit the feed information and click on the "Edit" button.
        </Typography>
        <Formik
            initialValues={{
                ...currentFeed
            }}
            onSubmit={handleOnEdit}
            validationSchema={feedSchema}
        >
            {
                ({
                    handleSubmit,
                    isSubmitting
                }) => (
                    <Box
                        className="flex flex-col gap-4 mt-6"
                    >
                        <TextFieldStyled
                            name="title"
                            label="Title"
                            type="text"
                            placeholder='Write a title'
                            disabled={isSubmitting}
                            maxLenght={25}
                        />
                        <TextFieldStyled
                            name="description"
                            label="Description"
                            type="text"
                            placeholder='Write a description'
                            multiline={true}
                            rows={4}
                            disabled={isSubmitting}
                            maxLenght={75}
                        />
                        <Box
                            className='flex flex-row gap-2'
                        >
                            
                            <Button
                                color='primary'
                                variant='outlined'
                                onClick={
                                    () => {
                                        onShowEdit()
                                    }
                                }
                                disabled={isSubmitting}
                                fullWidth
                            >
                                Cancel
                            </Button>
                            <Button
                                color='primary'
                                variant='contained'
                                onClick={
                                    () => handleSubmit()
                                }
                                disabled={isSubmitting}
                                type="submit"
                                fullWidth
                            >
                                {
                                    isSubmitting
                                        ? <CircularProgress size={20} />
                                        : 'Edit'
                                }
                            </Button>
                        </Box>
                    </Box>
                )
            }
        </Formik>
    </ModalStyled>
  )
}
