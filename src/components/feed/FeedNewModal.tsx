import Typography from '@mui/material/Typography'
import React from 'react'
import { ModalStyled } from '../ui/ModalStyled'
import { Formik } from 'formik'
import { Box } from '@mui/system'
import TextFieldStyled from '../ui/TextFieldStyled'
import Button from '@mui/material/Button/Button'
import { useSelector } from 'react-redux/es/exports'
import { useAppDispatch } from '../../redux/store'
import { clearNewFeed, selectNewFeed, _feedAddNewFeed } from '../../redux/slices/feedSlice'
import { FeedBaseProps } from '../../interface'
import { feedSchema } from '../../utils/schemas'
import CircularProgress from '@mui/material/CircularProgress'
export default function FeedNewModal() {

    const dispatch = useAppDispatch()
    const currentFeed =  useSelector<boolean|null>(selectNewFeed)
    
    const onShow = () => {
        dispatch(clearNewFeed())
    } 

    const handleOnSubmit = (
        values: FeedBaseProps,
        { setSubmitting }: any
    ) => {
        setSubmitting(true)
        dispatch(
            _feedAddNewFeed({
                ...values,
                date: new Date().getTime()
            })
        )
            .then(() => {
                onShow()
            })
            .catch((err) => {
                console.log(err)
            })
            .finally(() => {
                setSubmitting(false)
            })
    }


  return (
    <ModalStyled
        show={!!currentFeed}
        onShow={onShow}
    >
        <Typography 
            gutterBottom 
            variant="subtitle2"
            component="div"
            className='font-bold uppercase bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-purple-500'
        >
            Create a new post
        </Typography>
        <Typography 
            variant="body2" 
            color="text.secondary"
        >
            This a new post, write a title and a description and click on the "Create" button.
        </Typography>
        <Formik
            initialValues={{
                title: "",
                description: "",
            }}
            onSubmit={handleOnSubmit}
            validationSchema={feedSchema}
        >
            {
                ({
                    handleSubmit,
                    resetForm,
                    isSubmitting,
                    errors
                }) => (
                    <Box
                        className="flex flex-col gap-4 mt-6"
                    >
                        <TextFieldStyled
                            name="title"
                            label="Title"
                            type="text"
                            placeholder='Write a title'
                            disabled={isSubmitting}
                            maxLenght={25}
                        />
                        <TextFieldStyled
                            name="description"
                            label="Description"
                            type="text"
                            placeholder='Write a description'
                            multiline={true}
                            rows={4}
                            disabled={isSubmitting}
                            maxLenght={75}
                        />
                        <Box
                            className='flex flex-row gap-2'
                        >
                        
                            <Button
                               color='primary'
                               variant='outlined'
                                onClick={
                                    () => {
                                        resetForm()
                                        onShow()
                                    }
                                }
                                disabled={isSubmitting}
                                fullWidth
                            >
                                Cancel
                            </Button>
                            <Button
                                color='primary'
                                variant='contained'
                                onClick={
                                    () => handleSubmit()
                                }
                                disabled={
                                    isSubmitting
                                }
                                type="submit"
                                fullWidth
                            >
                                {
                                    isSubmitting
                                        ? <CircularProgress size={20} />
                                        : 'Create'
                                }
                            </Button>
                        </Box>
                    </Box>
                )
            }
        </Formik>
    </ModalStyled>
  )
}
