import Button from '@mui/material/Button/Button'
import Typography from '@mui/material/Typography'
import { useState } from 'react'
import { clearRemoveFeed, selectRemoveFeed, _feedDeleteFeedById } from '../../redux/slices/feedSlice'
import { useAppDispatch } from '../../redux/store'
import { ModalStyled } from '../ui/ModalStyled'
import { useSelector } from 'react-redux/es/exports'
import { FeedBaseProps } from '../../interface'
import Box from '@mui/material/Box/Box'
import CircularProgress from '@mui/material/CircularProgress'

export default function FeedDeleteModal() {

    const dispatch = useAppDispatch()
    const currentFeed =  useSelector<FeedBaseProps, FeedBaseProps>(selectRemoveFeed)

    const [loading, setLoading] = useState<boolean>(false)

    const onShowDelete = () => {
        dispatch(clearRemoveFeed())
    }

    const handleOnDelete = () => {
        setLoading(true)
        dispatch(_feedDeleteFeedById(currentFeed))
          .then(() => {
            onShowDelete()
          })
          .catch((err) => {
            console.log(err)
          })
          .finally(() => {
            setLoading(false)
          })
    }

  return (
    <ModalStyled
        show={!!currentFeed}
        onShow={onShowDelete}
    >
        <Box
            className='flex flex-row gap-2 mb-4 w-full sm:w-10/12 mx-auto'
        >
            <Typography
                gutterBottom 
                variant="h6"
                component="div"
                className='font-bold text-center uppercase bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-purple-500'
            >
                Are you sure you want to delete this item?
            </Typography>
        </Box>
        <Box
            className='flex flex-row w-full gap-2'
        >
            <Button
                onClick={handleOnDelete}
                color='primary'
                variant='outlined'
                disabled={loading}
                fullWidth
            >
                {
                    loading
                        ? <CircularProgress size={20} />
                        : 'Yes'
                }
            </Button>
            <Button
                onClick={onShowDelete}
                color='primary'
                variant='contained'
                disabled={loading}
                fullWidth
            >
                No
            </Button>
        </Box>
    </ModalStyled>
  )
}
