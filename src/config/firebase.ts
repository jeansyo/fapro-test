// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDApynw_vkcWSAQjeQKWYa2_0XO0DyHejU",
  authDomain: "tokyo-209bd.firebaseapp.com",
  projectId: "tokyo-209bd",
  storageBucket: "tokyo-209bd.appspot.com",
  messagingSenderId: "743339578146",
  appId: "1:743339578146:web:72c5e6765f21de5adbcb7b",
  measurementId: "G-ZFPJ4WXPN1"
};

// Initialize Firebase


const app = initializeApp(firebaseConfig);
export const database = getFirestore(app)