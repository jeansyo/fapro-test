import { useEffect, Fragment } from 'react'
import { Box } from '@mui/system'
import CardStyled from './components/ui/CardStyled'
import LayoutStyled from './components/ui/LayoutStyled'
import {
  motion,
  AnimatePresence
} from 'framer-motion'
import { useAppDispatch } from './redux/store'
import { selectAllFeedItems, selectLoadingFeed, _feedGetAllFeed } from './redux/slices/feedSlice'
import { useSelector } from 'react-redux/es/exports'
import FeedDeleteModal from './components/feed/FeedDeleteModal'
import FeedEditModal from './components/feed/FeedEditModal'
import FeedNewModal from './components/feed/FeedNewModal'
import { FeedBaseProps } from './interface'
import { LoadingStyled } from './components/ui/LoadingStyled'
import Typography from '@mui/material/Typography'

const variants = {
  hidden: { opacity: 0 },
  show: {
    opacity: 1,
    transition: {
      staggerChildren: 0.2,
    }
  }
}

function App() {

  const dispatch = useAppDispatch()
  const docs = useSelector<FeedBaseProps[], FeedBaseProps[]>(selectAllFeedItems)


  useEffect(() => {
    const promise = dispatch(_feedGetAllFeed())

    return () => {
      promise.abort()
    }

  }, [])

  return (
    <LayoutStyled>
      <Box
        className="flex flex-col gap-2 h-[85vh] sm:h-[73vh] overflow-y-auto overflow-x-visible flex-shrink-0 position-relative py-2 px-2"
        component={motion.div}
        variants={variants}
        initial="hidden"
        animate="show"
      >
        <FeedDeleteModal />
        <FeedEditModal />
        <FeedNewModal />

        {
          docs.length === 0 
            ? (
              <LoadingStyled>
                <Typography
                  variant="h5"
                  className="text-center pt-[3rem]"
                  color="text.secondary"
                >
                  No data found
                </Typography>
              </LoadingStyled>
            ): (
              <AnimatePresence>
                {
                  docs.map((prev:FeedBaseProps) => (
                    <CardStyled
                      key={prev.id}
                      {
                        ...prev
                      }
                    />
                  ))
                }
              </AnimatePresence>
            )
        }

      </Box>
    </LayoutStyled>
  )
}

export default App
