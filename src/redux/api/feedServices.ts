import { addDoc, collection, deleteDoc, doc, getDocs, updateDoc } from "firebase/firestore";
import { database } from "../../config/firebase";
import { FeedBaseProps, JeanSyoResponse } from "../../interface";


export const getDataFeedService = (): Promise<JeanSyoResponse> => {

    return new Promise((resolve, reject) => {
        getDocs(
            collection(
                database,
                "feed"
            )
        )
                .then(res=>{
                    const newData:Array<FeedBaseProps> = res.docs.map(
                        (doc:any) => ({
                            ...doc.data(), 
                            id: doc.id
                        })
                    );
                    resolve({
                        "jeansyo": {
                            date: new Date(),
                            code: 200,
                            message: "Data JSON upload successfully",
                            success: true,
                            data: newData
                        }
                    })
                })
                .catch((err)=>{
                    reject({
                        "jeansyo": {
                            date: new Date(),
                            code: 400,
                            message: "An error occurred data JSON upload, please try again",
                            success: false
                        }
                    })
                })
    })
}

export const setDataFeedService = (data: FeedBaseProps):Promise<JeanSyoResponse> => {
    return new Promise((resolve, reject) => {
        addDoc(
            collection(database, 'feed'),
            data
        )
        .then((res)=>{
            resolve({
                "jeansyo": {
                    date: new Date(),
                    code: 200,
                    message: "Data JSON upload successfully",
                    success: true,
                    data: {
                        ...data,
                        id: res.id
                    },
                }
            })
        })
        .catch(
            (err)=>{
                reject({
                    "jeansyo": {
                        date: new Date(),
                        code: 400,
                        message: "An error occurred data JSON upload, please try again",
                        success: false
                    }
                })
            }
        )
    })
}


export const deleteDataFeedService = (data: FeedBaseProps):Promise<JeanSyoResponse> => {
    return new Promise(
        (resolve, reject) => {
            deleteDoc(
                doc(
                    database,
                    "feed",
                    data.id || ""
                )
            )
                .then((_) => {
                    resolve({
                        "jeansyo": {
                            date: new Date(),
                            code: 200,
                            message: "Data JSON delete successfully",
                            success: true
                        }
                    })
                })
                .catch(err => {
                    reject({
                        "jeansyo": {
                            date: new Date(),
                            code: 400,
                            success: false,
                            message: "An error occurred data JSON delete, please try again"
                        }
                    })
                })
        });
}


export const updateDataFeedService = ({
    id,
    ...rest
}: FeedBaseProps):Promise<JeanSyoResponse> => {

    return new Promise((resolve, reject) => {
        updateDoc(
            doc(
                database,
                "feed",
                id||""
            ),
            {
                ...rest
            }
        )
            .then((_) => {
                resolve({
                    "jeansyo": {
                        date: new Date(),
                        code: 200,
                        message: "Data JSON update successfully",
                        success: true
                    }
                })
            })
            .catch(err => {
                reject({
                    "jeansyo": {
                        date: new Date(),
                        code: 400,
                        message: "An error occurred data JSON update, please try again",
                        success: false
                    }
                })
            })

    })
}