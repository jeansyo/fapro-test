import { configureStore } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import { feedSlice } from './slices/feedSlice'


const store = configureStore({  
    reducer: {
        feed: feedSlice.reducer,
    },
    devTools: true,
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch

export type RootState = ReturnType<typeof store.getState>
export default store