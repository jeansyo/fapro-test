import { createAsyncThunk, createEntityAdapter, createSelector, createSlice } from '@reduxjs/toolkit'
import { FeedBaseProps, JeanSyoResponse } from '../../interface';
import { deleteDataFeedService, getDataFeedService, setDataFeedService, updateDataFeedService } from '../api/feedServices';

export const feedAdapter = createEntityAdapter<any>({
    selectId: (item) => item.id,
    sortComparer: (a, b) => b.date - a.date,
})

const feedSelectors = feedAdapter.getSelectors((state: any) => state.feed)
const {
    selectAll: selectAllFeed,
    selectById: selectFeedById,
    selectIds: selectFeedIds,
} = feedSelectors;



export const _feedGetAllFeed = createAsyncThunk(
    'feed/getAllFeed',
    async (
        _,
        thunkAPI: any,
    ) => {
        try {

            const {
                jeansyo
            }:JeanSyoResponse = await getDataFeedService()


            return jeansyo.success ? jeansyo.data : []

        } catch (error) {
            return thunkAPI.rejectWithValue(error)
        }
    }
)

export const _feedDeleteFeedById = createAsyncThunk(
    'feed/deleteFeedById',
    async (
        payload: FeedBaseProps,
        thunkAPI: any
    ) => {
        try {

            const {
                jeansyo
            }: JeanSyoResponse = await deleteDataFeedService(payload)

            return jeansyo.success ? payload.id : null

        } catch (error) {
            return thunkAPI.rejectWithValue(error)
        }
    }
)

export const _feedAddNewFeed = createAsyncThunk(
    'feed/addNewFeed',
    async (
        payload: FeedBaseProps,
        thunkAPI: any, 
    ) => {
        try {

            const {
                jeansyo
            }: JeanSyoResponse = await setDataFeedService(payload)

            return jeansyo.success ? { ...jeansyo.data } : {}

        } catch (error) {
            return thunkAPI.rejectWithValue(error)
        }
    }
)

export const _feedUpdateFeedById = createAsyncThunk(
    'feed/updateFeedById',
    async (
        payload: FeedBaseProps,
        thunkAPI: any,
    ) => {
        try {

            const {
                jeansyo
            }: JeanSyoResponse = await updateDataFeedService(payload)

            return jeansyo.success ? payload : null

        } catch (error) {
            return thunkAPI.rejectWithValue(error)
        }
    }
)

export const selectAllFeedItems = createSelector(
    selectAllFeed,
    (feed) => feed
)

export const selectFeedByIdItem = createSelector(
    selectFeedById,
    (feed) => feed
)

// selector editFeed
export const selectEditFeed = createSelector(
    (state: any) => state.feed.editFeed,
    (editFeed) => editFeed
)

export const selectRemoveFeed = createSelector(
    (state: any) => state.feed.removeFeed,
    (removeFeed) => removeFeed
)

export const selectNewFeed = createSelector(
    (state: any) => state.feed.newFeed,
    (newFeed) => newFeed
)

export const selectLoadingFeed = createSelector(
    (state: any) => state.feed.loading,
    (loading) => loading
)

export const feedSlice = createSlice({
    name: 'feed',
    initialState: feedAdapter.getInitialState({
        editFeed: null,
        removeFeed: null,
        newFeed: null,
        loading: true,
    }),
    reducers: {
        setEditFeed: (state, action) => {
            state.editFeed = action.payload
        },
        setRemoveFeed: (state, action) => {
            state.removeFeed = action.payload
        },
        clearEditFeed: (state) => {
            state.editFeed = null
        },
        clearRemoveFeed: (state) => {
            state.removeFeed = null
        },
        setNewFeed: (state, action) => {
            state.newFeed = action.payload
        },
        clearNewFeed: (state) => {
            state.newFeed = null
        }
    },
    extraReducers: {
        [_feedGetAllFeed.pending.type]: (state) => {
            state.loading = true
        },
        [_feedGetAllFeed.fulfilled.type]: (state, action) => {
            feedAdapter.setAll(state, action.payload)
            state.loading = false
        },
        [_feedGetAllFeed.rejected.type]: (state) => {
            state.loading = false
        },
        [_feedDeleteFeedById.fulfilled.type]: feedAdapter.removeOne,
        [_feedAddNewFeed.fulfilled.type]: feedAdapter.addOne,
        [_feedUpdateFeedById.fulfilled.type]: feedAdapter.upsertOne,
    }
})

export const {
    setEditFeed,
    setRemoveFeed,
    clearEditFeed,
    clearRemoveFeed,
    setNewFeed,
    clearNewFeed,
} = feedSlice.actions